# Thirdparty Packages

Here, you'll find a curated collection of package spec files, scripts, and patches. These packages include the latest stable, development, and testing versions, specially tailored to match "RHEL, Centos & Fedora" operating systems.



## Repository Overview

In this repository, we use TOML (Tom's Obvious, Minimal Language) files for package specifications and patch management. TOML enables us to define package properties and dependencies in a flexible and human-readable format, allowing for seamless integration into various systems.



- `branches.toml`: This TOML file is used for managing branches or variants of packages in the repository. It contains information about different package versions and customizations to cater to specific requirements. Branches (Stable "RHEL", Development "Centos", Testing "Fedora")

- `b`,`c`, `d`, `j`, `k`, `l`, `n`, `p`, `q`, `r`, `s`, `t`, `v`, `y`: These directories represent individual packages or groups of related packages. Each directory contains a package's TOML spec file, detailing its properties, dependencies, and patches. Additionally, you may find other relevant files, such as documentation and additional resources.

- `groups.toml`: This TOML file defines package groups, allowing us to organize related packages together, making it easier for users to install multiple packages with a single command.

  
