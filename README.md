# SquidFX

SquidFX is an innovative and community-driven platform  dedicated to nurturing talent in the world of film and television  production. Our mission is to democratize the creative process by  providing a robust suite of resources, tools, and opportunities for both aspiring and experienced filmmakers and producers. We believe in  bringing dreams to the screen by making the tools of the trade  accessible to everyone, regardless of economic circumstances.

At the heart of SquidFX is a comprehensive library of  learning materials, assets, and software tailored to each stage of  production, from pre-production through to delivery. Users can expect a  dynamic, hands-on learning experience with access to training materials  developed by industry veterans, an expansive library of assets, and a  suite of cutting-edge tools. Further, SquidFX provides a unique  opportunity to work on real-world projects under the mentorship of  experienced professionals.

Our vision is to become the ultimate hub for film and  television production, enabling the creation of diverse content ranging  from full-length movies and television series to news broadcasts and  documentaries. We aim to provide the keys to unlock the creative  potential within each member of our community, fostering a new  generation of innovative filmmakers and producers.



## About the SDK

The SDK (Software Development Kit) is a comprehensive and powerful  toolset provided by SquidFX, designed to facilitate the development of  applications and projects using their SquidFX Qt C++ library. The SDK  offers a range of essential resources and utilities to streamline the  development process and enhance productivity.

| Name           | Description                                                  |
| -------------- | ------------------------------------------------------------ |
| build          | This directory is used to build the project, keeping the root directory clean. |
| docs           | Blazor documentation server for the SDK project.             |
| modules        | C# (P/Invoke) bindings for the SquidFX Qt C++ library's. |
| resources      | Fonts, Icons, Mouse Cursor themes for Qt.                    |
| src            | SquidFX Qt C++ library's.                                      |
| templates      | .NET C# project templates (see: dotnet new --list).          |
| thirdparty     | C++ dependencies for the SquidFX Qt C++ library.             |
| tools          | Custom tools developed by SquidFX or the SDK.                |
| CMakeLists.txt | C++ build system.                                            |
| LICENSE        | MIT license that applies to all code in the SDK written by SquidFX. |
| SDK.sln        | C# build system.                                             |
