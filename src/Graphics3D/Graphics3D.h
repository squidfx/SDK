#ifndef ARCHIVE_H
#define ARCHIVE_H

#include "Archive_global.h"

#include <QString>

class ARCHIVE_EXPORT Archive
{
public:
    Archive();
    bool open(const QString& filePath);
};

#endif // ARCHIVE_H
