#include "Archive.h"

#include <cstdio>
#include <sys/stat.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <archive.h>
#include <archive_entry.h>

Archive::Archive()
{
}

bool Archive::open(const QString& filePath)
{
    // Convert the QString to a C-string
    QByteArray byteArray = filePath.toUtf8();
    const char* archivePath = byteArray.constData();

    // Open the archive file
    struct archive* arch = archive_read_new();
    archive_read_support_format_all(arch);
    int result = archive_read_open_filename(arch, archivePath, 10240); // Use an appropriate block size

    if (result != ARCHIVE_OK)
    {
        // Failed to open the archive
        archive_read_free(arch);
        return false;
    }

    // Get some information about the archive
    struct archive_entry* entry;
    const char* entryPath;
    qint64 totalSize = 0;
    int entryCount = 0;

    while (archive_read_next_header(arch, &entry) == ARCHIVE_OK)
    {
        entryPath = archive_entry_pathname(entry);
        totalSize += archive_entry_size(entry);
        entryCount++;
    }

    // Store the information about the archive in your class variables or do whatever you need with it
    // For example, you could emit signals with the information or store it in member variables.
    // Here, we'll just print it for demonstration purposes.

    printf("Archive Path: %s\n", filePath.toUtf8().constData());
    printf("Number of Entries: %d\n", entryCount);
    printf("Total Size: %lld bytes\n", totalSize);


    // Close the archive and free the resources
    archive_read_close(arch);
    archive_read_free(arch);

    return true;
}
