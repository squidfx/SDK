#ifndef EASING_H
#define EASING_H

#include "Easing_global.h"

#include <QString>

class EASING_EXPORT Easing
{
public:
    Easing();
    bool open(const QString& filePath);
};

#endif // EASING_H
