#ifndef EASING_GLOBAL_H
#define EASING_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(EASING_LIBRARY)
#  define EASING_EXPORT Q_DECL_EXPORT
#else
#  define EASING_EXPORT Q_DECL_IMPORT
#endif

#endif // EASING_GLOBAL_H
