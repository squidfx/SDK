cmake_minimum_required(VERSION 3.14)

project(Archive LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core)

# Find LibArchive package
find_package(LibArchive REQUIRED)

add_library(${PROJECT_NAME} SHARED
  Archive_global.h
  Archive.cpp
  Archive.h
)

target_link_libraries(${PROJECT_NAME} PRIVATE Qt${QT_VERSION_MAJOR}::Core LibArchive::LibArchive)

# Add zlib and bzip2 to the linker flags
target_link_libraries(${PROJECT_NAME} PRIVATE -lz -lbz2)

target_compile_definitions(${PROJECT_NAME} PRIVATE ARCHIVE_LIBRARY)
