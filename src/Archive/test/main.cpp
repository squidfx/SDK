// main.cpp
#include <QCoreApplication>
#include "../archive.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    if (argc != 2) {
        qCritical() << "Usage: example_archive_project <path_to_archive>";
        return 1;
    }

    Archive archive;
    if (!archive.open(QString::fromUtf8(argv[1]))) {
        qCritical() << "Failed to open the archive.";
        return 1;
    }

    return 0;
}
