#ifndef ARCHIVE_GLOBAL_H
#define ARCHIVE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(ARCHIVE_LIBRARY)
#  define ARCHIVE_EXPORT Q_DECL_EXPORT
#else
#  define ARCHIVE_EXPORT Q_DECL_IMPORT
#endif

#endif // ARCHIVE_GLOBAL_H
