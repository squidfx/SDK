#ifndef DATEEDIT_GLOBAL_H
#define DATEEDIT_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DATEEDIT_LIBRARY)
#  define DATEEDIT_EXPORT Q_DECL_EXPORT
#else
#  define DATEEDIT_EXPORT Q_DECL_IMPORT
#endif

#endif // DATEEDIT_GLOBAL_H
