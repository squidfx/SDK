#ifndef FLEXBOX_GLOBAL_H
#define FLEXBOX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(FLEXBOX_LIBRARY)
#  define FLEXBOX_EXPORT Q_DECL_EXPORT
#else
#  define FLEXBOX_EXPORT Q_DECL_IMPORT
#endif

#endif // FLEXBOX_GLOBAL_H
