#ifndef CHECKBOX_GLOBAL_H
#define CHECKBOX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CHECKBOX_LIBRARY)
#  define CHECKBOX_EXPORT Q_DECL_EXPORT
#else
#  define CHECKBOX_EXPORT Q_DECL_IMPORT
#endif

#endif // CHECKBOX_GLOBAL_H
