#ifndef COMBOBOX_GLOBAL_H
#define COMBOBOX_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(COMBOBOX_LIBRARY)
#  define COMBOBOX_EXPORT Q_DECL_EXPORT
#else
#  define COMBOBOX_EXPORT Q_DECL_IMPORT
#endif

#endif // COMBOBOX_GLOBAL_H
